<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
class CategoriesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Category';

    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form(true)->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category());

        $grid->id('ID')->sortable();
        $grid->name('Name');
        $grid->level('Level');
        $grid->is_directory('Is_Dir')->display(function($value) {
            return $value ? 'Yes' : 'No';
        });
        $grid->path('Path');

        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($isEditing = false)
    {
        $form = new Form(new Category());

        $form->text('name', 'Category Name')->rules('required');

        if($isEditing){
            $form->display('is_directory', 'Is_Dir')->with(function ($value) {
                return $value ? 'Yes' : 'No';
            });

            $form->display('parent.name', 'Parent Category');
        } else {
            $form->radio('is_directory', 'Is_Dir')
                ->options(['1' => 'Yes', '0' => 'No'])
                ->default('0')
                ->rules('required');
                
            $form->select('parent_id', 'Parent Category')->ajax('/admin/api/categories');
        }

        return $form;
    }

    public function apiIndex(Request $request) {
        $search = $request->input('q');
        $result = Category::query()
            ->where('is_directory', true)
            ->where('name', 'like', '%'.$search.'%')
            ->paginate();

         $result->setCollection($result->getCollection()->map(function (Category $category) {
            return ['id' => $category->id, 'text' => $category->full_name];
        }));

        return $result;
    }
}
