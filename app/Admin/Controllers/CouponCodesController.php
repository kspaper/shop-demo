<?php

namespace App\Admin\Controllers;

use App\Models\CouponCode;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class CouponCodesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Coupon Code List';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CouponCode());

        $grid->model()->orderBy('created_at', 'desc');
        $grid->id('ID')->sortable();
        $grid->name('Coupon Name');
        $grid->code('Coupon Code');
        $grid->description('Description');
        $grid->column('usage', 'Remaining')->display(function ($value) {
            return "{$this->used} / {$this->total}";
        });
        $grid->enabled('Enabled')->display(function($value) {
            return $value ? 'Yes' : 'No';
        });
        $grid->created_at('Create Date');
        $grid->not_before('Start Date');
        $grid->not_after('Due Date');
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    protected function form()
    {
        $form = new Form(new CouponCode());

        $form->display('id', 'ID');
        $form->text('name', 'Coupon Name')->rules('required');
        $form->text('code', 'Coupon Code')->rules(function ($form) {
            if($id = $form->model()->id) {
                return 'nullable|unique:coupon_codes,code,'.$id.',id';
            } else {
                return 'nullable|unique:coupon_codes';
            }
        });
        $form->radio('type', 'Coupon Type')->options(CouponCode::$typeMap)->rules('required')->default(CouponCode::TYPE_FIXED);
        $form->text('value', 'Coupon Value')->rules(function ($form) {
            if (request()->input('type') === CouponCode::TYPE_PERCENT) {
                return 'required|numeric|between:1,99';
            } else {
                return 'required|numeric|min:0.01';
            }
        });
        $form->text('total', 'Total')->rules('required|numeric|min:1');
        $form->text('min_amount','Minimal Amount')->rules('required|numeric|min:0');
        $form->datetime('not_before', 'Start Date');
        $form->datetime('not_after', 'End Date');
        $form->switch('enabled', 'Enabled')->options(['1' => 'Yes', '0' => 'No']);

        $form->saving(function (Form $form) {
            if(!$form->code) {
                $form->code = CouponCode::findAvailableCode();
            }
        });

        return $form;
    }
}
