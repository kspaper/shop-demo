<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class OrdersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Order';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order());

        $grid->model()->whereNotNull('paid_at')->orderBy('paid_at', 'desc');

        $grid->no('Order No.');
        $grid->column('user.name', 'Buyer');
        $grid->total_amount('Total Amount')->sortable();
        $grid->paid_at('Payment Date')->sortable();
        // $grid->ship_status('Shipping')->display(function($value) {
        //     return Order::$shipStatusMap[$value];
        // });
        $grid->refund_status('Refund Status')->display(function($value) {
            return Order::$refundStatusMap[$value];
        });

        $grid->disableCreateButton();

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableEdit();
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    public function show($id, Content $content)
    {
        return $content
            ->header('View Order')
            ->body(view('admin.orders.show', ['order' => Order::find($id)]));
    }
}
