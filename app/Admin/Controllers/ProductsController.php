<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProductsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Item';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());

        $grid->id('ID')->sortable();
        $grid->title('Title');
        $grid->on_sale('Published')->display(function ($value) {
            return $value ? 'Yes' : 'No';
        });
        $grid->price('Price');
        $grid->rating('Rating');
        $grid->sold_count('Sold');
        $grid->review_count('Reviews');

        $grid->actions(function ($actions){
            $actions->disableView();
            $actions->disableDelete();
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());

        $form->text('title', 'Title')->rules('required');
        $form->image('image', 'Cover')->rules('required|image');
        $form->quill('description', 'Description')->rules('required');
        $form->radio('on_sale', 'Published')->options(['1' => 'Yes', '0' => 'No'])->default('0');

        $form->hasMany('skus', 'SKU List', function (Form\NestedForm $form){
            $form->text('title', 'SKU Title')->rules('required');
            $form->text('description', 'SKU Description')->rules('required');
            $form->text('price', 'Price')->rules('required|numeric|min:0.01');
            $form->text('stock', 'Remaining')->rules('required|integer|min:0');
        });

        $form->saving(function (Form $form){
            $form->model()->price = collect($form->input('skus'))->where(Form::REMOVE_FLAG_NAME, 0)->min('price') ? : 0;
        });

        return $form;
    }
}
