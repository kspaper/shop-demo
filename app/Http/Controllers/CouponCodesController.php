<?php

namespace App\Http\Controllers;

use App\Exceptions\CouponCodeUnavailableException;
use App\Models\CouponCode;


class CouponCodesController extends Controller
{
    public function show($code)
    {
        // Check if code exists
        if (!$record = CouponCode::where('code', $code)->first()) {
            throw new CouponCodeUnavailableException('The coupon is not existed');
        }

        $record->checkAvailable();

        return $record;
    }
}
