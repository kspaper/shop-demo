<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidRequestException;
use Illuminate\Http\Request;
use App\Models\Order;
use Carbon\Carbon;
use App\Events\OrderPaid;

class PaymentController extends Controller
{
    public function generateInvoice(Order $order) {
        $this->authorize('own', $order);

        return view('payment.invoice', ['order' => $order]);
    }
    public function simulatePay(Order $order, Request $request)
    {
        $this->authorize('own', $order);

        if ($order->paid_at || $order->closed) {
            throw new InvalidRequestException('The order is no longer available');
        }

        $payment_method = $request->input('payment_method');
        $order->update([
            'paid_at' => Carbon::now(),
            'payment_method' => $payment_method,
            'payment_no' => 'SIM-' . $order->no
        ]);

        event(new OrderPaid($order));

        return $order;
    }
}
