<?php

namespace App\Http\Requests;

use App\Models\ProductSku;

class AddCartRequest extends Request
{
    public function rules()
    {
        return [
            'sku_id' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!$sku = ProductSku::find($value)) {
                        return $fail('The product is not existed');
                    }
                    if (!$sku->product->on_sale) {
                        return $fail('The product has not published');
                    }
                    if ($sku->stock === 0) {
                        return $fail('The product has sold out');
                    }
                    if ($this->input('amount') > 0 && $sku->stock < $this->input('amount')) {
                        return $fail('The stock remaining is insufficient');
                    }
                },
            ],
            'amount' => ['required', 'integer', 'min:1'],
        ];
    }

    public function attributes()
    {
        return [
            'amount' => 'Quantity'
        ];
    }

    public function messages()
    {
        return [
            'sku_id.required' => 'Please pick a SKU'
        ];
    }
}
