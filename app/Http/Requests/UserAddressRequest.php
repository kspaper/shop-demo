<?php

namespace App\Http\Requests;

class UserAddressRequest extends Request
{
    public function rules()
    {
        return [
            'region'	=> 'required',
	        'state' 	=> 'required',
	        'city' 	    => 'required',
	        'address' 	=> 'required',
	        'zip' 	    => 'required',
	        'contact_name'  => 'required',
	        'contact_phone' => 'required',
        ];
    }
}
