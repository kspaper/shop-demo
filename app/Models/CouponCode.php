<?php

namespace App\Models;

use App\Exceptions\CouponCodeUnavailableException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use Illuminate\Support\Str;

class CouponCode extends Model
{
    use HasFactory, DefaultDatetimeFormat;

    const TYPE_FIXED = 'fixed';
    const TYPE_PERCENT = 'percent';

    public static $typeMap = [
        self::TYPE_FIXED   => 'Dollars',
        self::TYPE_PERCENT => 'Percent',
    ];

    protected $fillable = [
        'name',
        'code',
        'type',
        'value',
        'total',
        'used',
        'min_amount',
        'not_before',
        'not_after',
        'enabled',
    ];

    protected $casts = [
        'enabled' => 'boolean',
    ];

    protected $dates = ['not_before', 'not_after'];

    protected $appends = ['description'];

    // Test purpose to generate unique code
    public static function findAvailableCode($length = 16)
    {
        do {
            $code = strtoupper(Str::random($length));
        } while (self::query()->where('code', $code)->exists());

        return $code;
    }

    // Regulate display layout in Coupon Management
    public function getDescriptionAttribute()
    {
        $str = '';

        if ($this->min_amount > 0) {
            $str = 'SPEND OVER ' . str_replace('.00', '', $this->min_amount);
        }
        if ($this->type === self::TYPE_PERCENT) {
            return $str . ' GET ' . str_replace('.00', '', $this->value) . '% OFF';
        }

        return $str . ' SAVE $' . str_replace('.00', '', $this->value);
    }

    // Validate the coupon
    public function checkAvailable($orderAmount = null)
    {
        // Check if code enabled
        if (!$this->enabled) {
            throw new CouponCodeUnavailableException('The coupon is unavailable yet');
        }

        // Check if code has used up
        if ($this->total - $this->used <= 0) {
            throw new CouponCodeUnavailableException('The coupon has used up');
        }

        // not_before: 2021-8-2         Carbon::now: 2021-8-1
        if ($this->not_before && $this->not_before->gt(Carbon::now())) {
            throw new CouponCodeUnavailableException('The coupon has not started yet');
        }

        // not_before: 2021-8-2         Carbon::now: 2021-8-3
        if ($this->not_after && $this->not_after->lt(Carbon::now())) {
            throw new CouponCodeUnavailableException('The coupon has been expired');
        }

        if (!is_null($orderAmount) && $orderAmount < $this->min_amount) {
            throw new CouponCodeUnavailableException('The coupon requires minimum spends $'.$this->min_amount);
        }
    }

    // Calculate price with coupon value
    public function getAdjustPrice($orderAmount)
    {
        if($this->type === self::TYPE_FIXED) {
            return max(1, $orderAmount - $this->value);
        }

        return number_format($orderAmount * (100 - $this->value) / 100, 2, '.', '');
        //return $orderAmount * (100 - $this->value) / 100;
    }

    // Increase or decrease used coupon
    public function changeUsed($increase = true)
    {
        if ($increase) {
            return $this->where('id', $this->id)->where('used', '<', $this->total)->increment('used');
        } else {
            return $this->decrement('used');
        }
    }
}
