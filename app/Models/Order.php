<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Order extends Model
{
    use HasFactory, DefaultDatetimeFormat;

    const REFUND_STATUS_PENDING = 'pending';
    const REFUND_STATUS_APPLIED = 'applied';
    const REFUND_STATUS_PROCESSING = 'processing';
    const REFUND_STATUS_SUCCESS = 'success';
    const REFUND_STATUS_FAILED = 'failed';

    const SHIP_STATUS_PENDING = 'pending';
    const SHIP_STATUS_DELIVERED = 'delivered';
    const SHIP_STATUS_RECEIVED = 'received';

    public static $refundStatusMap = [
        self::REFUND_STATUS_PENDING    => 'Pending',
        self::REFUND_STATUS_APPLIED    => 'Applied',
        self::REFUND_STATUS_PROCESSING => 'Processing',
        self::REFUND_STATUS_SUCCESS    => 'Approved',
        self::REFUND_STATUS_FAILED     => 'Rejected',
    ];

    public static $shipStatusMap = [
        self::SHIP_STATUS_PENDING   => 'Pending',
        self::SHIP_STATUS_DELIVERED => 'Delivering',
        self::SHIP_STATUS_RECEIVED  => 'Received',
    ];

    protected $fillable = [
        'no',
        'address',
        'total_amount',
        'remark',
        'paid_at',
        'payment_method',
        'payment_no',
        'refund_status',
        'refund_no',
        'closed',
        'reviewed',
        'ship_status',
        'ship_data',
        'extra',
    ];

    protected $casts = [
        'closed'    => 'boolean',
        'reviewed'  => 'boolean',
        'address'   => 'json',
        'ship_data' => 'json',
        'extra'     => 'json',
    ];

    protected $dates = [
        'paid_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (!$model->no) {
                $model->no = static::findAvailableNo();
                if (!$model->no) {
                    return false;
                }
            }
        });
    }

    // Relation with User model
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Relation with OrderItem Model
    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    // Relation with CouponCode Model
    public function couponCode()
    {
        return $this->belongsTo(CouponCode::class);
    }

    public static function findAvailableNo()
    {
        $prefix = date('YmdHis');

        for ($i = 0; $i < 10; $i++) {
            $no = $prefix.str_pad(random_int(0, 999999), 6, '0', STR_PAD_LEFT);

            if (!static::query()->where('no', $no)->exists()) {
                return $no;
            }
        }
        \Log::warning('find order no. failed');

        return false;
    }
}
