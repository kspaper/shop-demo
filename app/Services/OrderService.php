<?php

namespace App\Services;

use App\Exceptions\CouponCodeUnavailableException;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\Order;
use App\Models\ProductSku;
use Carbon\Carbon;
use App\Exceptions\InvalidRequestException;
use App\Models\CouponCode;

Class OrderService {
    public function store(User $user, UserAddress $userAddress, $remark, $items, CouponCode $coupon = null)
    {
        if($coupon) {
            $coupon->checkAvailable();
        }

        $order = \DB::transaction(function () use ($user, $userAddress, $remark, $items, $coupon) {

            $userAddress->update(['last_used_at' => Carbon::now()]);

            $order = new Order([
                'address' => [
                    'address'   => $userAddress->full_address,
                    'zip'       => $userAddress->zip,
                    'contact_name'  => $userAddress->contact_name,
                    'contact_phone' => $userAddress->contact_phone,
                ],
                'remark' => $remark,
                'total_amount' => 0,
            ]);

             // for foreign key col. user_id
             $order->user()->associate($user);

             $order->save();

             $totalAmount = 0;

             foreach ($items as $data) {
                 $sku  = ProductSku::find($data['sku_id']);

                 $item = $order->items()->make([
                     'amount' => $data['amount'],
                     'price' => $sku->price,
                 ]);
                 $item->product()->associate($sku->product_id);
                 $item->productSku()->associate($sku);

                 $item->save();

                 $totalAmount += $sku->price * $data['amount'];
                 if ($sku->decreaseStock($data['amount']) <= 0) {
                     throw new InvalidRequestException('The stock remaining is insufficient');
                 }
             }

             if($coupon) {
                 // Check Coupon again after get total amount and calculate final price
                 $coupon->checkAvailable($totalAmount);
                 $totalAmount = $coupon->getAdjustPrice($totalAmount);
                 $order->couponCode()->associate($coupon);
                 if($coupon->changeUsed() <= 0) {
                     throw new CouponCodeUnavailableException('The coupon has been used up');
                 }
             }

             $order->update(['total_amount' => $totalAmount]);

            // Remove ordered items from cart
            $skuIds = collect($items)->pluck('sku_id');
            app(CartService::class)->remove($skuIds);

            return $order;
        });

        return $order;
    }
}

