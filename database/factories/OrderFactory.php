<?php

namespace Database\Factories;

use App\Models\CouponCode;
use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::query()->inRandomOrder()->first();

        $address = $user->addresses()->inRandomOrder()->first();

        $coupon = null;

        if (random_int(0, 10) < 3) {
            $coupon = CouponCode::query()->where('min_amount', 0)->inRandomOrder()->first();
            $coupon->changeUsed();
        }

        return [
            'user_id' => $user->id,
            'address'        => [
                'address'       => $address->full_address,
                'zip'           => $address->zip,
                'contact_name'  => $address->contact_name,
                'contact_phone' => $address->contact_phone,
            ],
            'total_amount'   => 0,
            'remark'         => $this->faker->sentence,
            'paid_at'        => $this->faker->dateTimeBetween('-30 days'),
            'coupon_code_id' => $coupon ? $coupon->id : null,
            'payment_method' => 'simulation',
            'payment_no'     => 'SIM-'.$this->faker->uuid(),
            'refund_status'  => 'pending',
            'refund_no'      => null,
            'closed'         => false,
            'reviewed'       => random_int(0, 10) > 2,
            'ship_status'    => 'pending',
            'ship_data'      => null,
            'extra'          => null,

        ];
    }
}
