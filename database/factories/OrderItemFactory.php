<?php

namespace Database\Factories;

use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $product = Product::query()->where('on_sale', true)->inRandomOrder()->first();

        $sku = $product->skus()->inRandomOrder()->first();

        return [
            'amount'         => random_int(1, 5),
            'price'          => $sku->price,
            'rating'         => null,
            'review'         => null,
            'reviewed_at'    => null,
            'product_id'     => $product->id,
            'product_sku_id' => $sku->id,
        ];
    }
}
