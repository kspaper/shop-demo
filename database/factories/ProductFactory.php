<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $image = $this->faker->randomElement([
            "images/hd.jpg",
            "images/desktop.jpg",
            "images/laptop.jpg",
            "images/headset.jpg",
            "images/printer.jpg",
            "images/monitor.jpg",
            "images/tablet.jpg",
            "images/router.jpg",
        ]);

        return [
            'title'         => $this->faker->word,
            'description'   => $this->faker->sentence,
            'image'         => $image,
            'on_sale'       => true,
            'rating'        => $this->faker->numberBetween(0,5),
            'sold_count'    => 0,
            'review_count'  => 0,
            'price'         => 0,
        ];
    }
}
