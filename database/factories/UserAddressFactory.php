<?php

namespace Database\Factories;

use App\Models\UserAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $addresses = [
            ["South East", "VIC", "Brighton"],
            ["South East", "VIC", "Melbourne"],
            ["Central East", "NSW", "Adelaide"],
            ["Central East", "NSW", "Sydney"],
            ["North East", "QLD", "Brisbane"],
        ];

        $address = $this->faker->randomElement($addresses);

        return [
            'region' => $address[0],
            'state' => $address[1],
            'city' => $address[2],
            'address' => sprintf('%d Test Str', $this->faker->randomNumber(2)),
            'zip' => $this->faker->postcode(),
            'contact_name' => $this->faker->name(),
            'contact_phone' => $this->faker->phoneNumber(),
        ];
    }
}
