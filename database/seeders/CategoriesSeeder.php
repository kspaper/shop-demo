<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            // --- Phone Accessories ---
            [
                'name'      =>  'Phone Accessories',
                'children'  => [
                    ['name' => 'Cases & Covers'],
                    ['name' => 'Screen Protectors'],
                    ['name' => 'Cables & Adapters'],
                    ['name' => 'Mounts & Holders'],
                    ['name' => 'Memory Cards'],
                    [
                        'name'      =>  'Headsets',
                        'children'  =>  [
                            ['name'  =>  'Bluetooth'],
                            ['name'  =>  'Lightning'],
                            ['name'  =>  'USB-C'],
                        ],
                    ],
                    ['name' => 'Chargers & Cradles'],
                ],
            ],
            // --- End ---
            // --- Computer Accessories ---
            [
                'name'      =>  'Computer Accessories',
                'children'  =>  [
                    ['name' =>  'Graphics/Video Cards'],
                    ['name' =>  'CPU / Processors'],
                    ['name' =>  'Memory(RAM)'],
                    ['name' =>  'Motherboards'],
                    ['name' =>  'Hard Disk']
                ],
            ],
            // --- End ---
            // --- Brand Computer ---
            [
                'name'      => 'Brand Computers',
                'children'  =>  [
                    ['name' =>  'Desktops'],
                    ['name' =>  'Laptops'],
                    ['name' =>  'Tablets'],
                    ['name' =>  'All-In-Ones'],
                    ['name' =>  'Servers'],
                ],
            ],
            // --- End ---
            // --- Mobile Phones ---
            [
                'name'      => 'Mobile Phones',
                'children'  =>  [
                    ['name' =>  'Apple'],
                    ['name' =>  'Samsung'],
                    ['name' =>  'Nokia'],
                ],
            ],
            // --- End ---
        ];

        foreach ($categories as $data) {
            $this->createCategory($data);
        }
    }

    protected function createCategory($data, $parent = null)
    {
        $category = new Category(['name' => $data['name']]);
        $category->is_directory = isset($data['children']);

        if (!is_null($parent)) {
            $category->parent()->associate($parent);
        }

        $category->save();

        if (isset($data['children']) && is_array($data['children'])) {
            foreach ($data['children'] as $child) {
                $this->createCategory($child, $category);
            }
        }
    }
}
