<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::factory()->count(20)->create();

        $products = collect([]);

        foreach ($orders as $order) {
            $items = OrderItem::factory()->count(random_int(1, 3))->create([
                'order_id'    => $order->id,
            ]);

            $total = $items->sum(function (OrderItem $item) {
                return $item->price * $item->amount;
            });

            $order->update([
                'total_amount' => $total,
            ]);

            $products = $products->merge($items->pluck('product'));
        }

        $products->unique('id')->each(function (Product $product) {

            $result = OrderItem::query()
                ->where('product_id', $product->id)
                ->whereHas('order', function ($query) {
                    $query->whereNotNull('paid_at');
                })
                ->first([
                    \DB::raw('avg(rating) as rating'),
                    \DB::raw('sum(amount) as sold_count'),
                ]);

            $product->update([
                'rating'       => $result->rating ?: 5,
                'sold_count'   => $result->sold_count,
            ]);
        });
    }
}
