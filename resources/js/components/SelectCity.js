// Loading data from customised file
const addressData = require('./data');
import _ from 'lodash';

Vue.component('select-city', {
    props: {
        initValue:{
            type: Array,
            default: () => ([]),
        }
    },
    // Define Data for region list, state list, city list
    // and selected region, selected state and seleceted city
    data(){
        return {
            regions: addressData['61'], // Get Region List
            states: {},
            cities: {},
            regionId: '',
            stateId: '',
            cityId: '',
        };
    },

    // Watcher to watch any change of selected item
    watch: {
        regionId(newVal){
            if(!newVal) {
                this.states = {};
                this.stateId = '';
                return;
            }
            this.states = addressData[newVal];
            if(!this.states[this.stateId]) {
                this.stateId = '';
            }
        },

        stateId(newVal){
            if(!newVal) {
                this.cities = {};
                this.cityId = '';
                return;
            }
            this.cities = addressData[newVal];
            if(!this.cities[this.cityId]) {
                this.cityId = '';
            }
        },

        cityId() {
            this.$emit('change', [this.regions[this.regionId], this.states[this.stateId], this.cities[this.cityId]]);
        },
    },

    created() {
        this.setFromValue(this.initValue);
    },

    methods: {
        setFromValue(value) {
            value = _.filter(value);

            if(value.length === 0) {
                this.regionId = '';
                return;
            }

            const regionId = _.findKey(this.regions, region => region === value[0]);

            if(!regionId) {
                this.regionId = '';
                return;
            }

            this.regionId = regionId;

            const stateId = _.findKey(addressData[regionId], state => state === value[1]);

            if(!stateId) {
                this.stateId = '';
                return;
            }

            this.stateId = stateId;

            const cityId = _.findKey(addressData[stateId], city => city === value[2]);

            if(!cityId) {
                this.cityId = '';
                return;
            }

            this.cityId = cityId;
        }
    }
}).default;




