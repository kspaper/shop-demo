Vue.component('user-addresses-create-and-edit', {
    data() {
        return {
            region: '',
            state: '',
            city: '',
        }
    },

    methods: {
        onCityChanged(val) {
            if(val.length === 3) {
                this.region = val[0];
                this.state = val[1];
                this.city = val[2];
            }
        }
    }
});
