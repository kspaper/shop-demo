<footer class="footer">
    <div class="container">
        <p class="float-left">
            Developed by Sean Kang </a>
        </p>

        <p class="float-right"><a href="mailto:sean.kangaroo@gmail.com">Email Me</a></p>
    </div>
</footer>
