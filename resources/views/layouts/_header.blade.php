<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-static-top">
    <div class="container">
        <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/') }}">
            Laravel Demo
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>
            <!-- Right Side of Navbar -->
            <ul class="navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}"> Login </a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}"> Signup </a></li>
                @else
                    <li class="nav-item">
                        <a class="nav-link mt-1" href="{{ route('cart.index') }}">
                            <i class="fa fa-shopping-cart"></i>  My Cart
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('img/icon.jpg') }}"
                                class="rounded-circle img-fluid user-img">
                                <span class="user-name">{{ Auth::user()->name }}</span>

                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a href="{{ route('user_addresses.index') }}" class="dropdown-item">Address List </a>
                            <a href="{{ route('orders.index') }}" class="dropdown-item">My Orders</a>
                            <a href="{{ route('products.favorites') }}" class="dropdown-item">My Favourites</a>
                            <a class="dropdown-item" id="logout" href="#"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>

                @endguest
                <!-- End of Authentication Links -->
            </ul>
        </div>
    </div>
</nav>
