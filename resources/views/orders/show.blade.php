@extends('layouts.app')
@section('title', 'Order Review')

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="card">
                <div class="card-header">
                    <h4>Order Review</h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Item Info.</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right item-amount">Cost</th>
                            </tr>
                        </thead>
                        @foreach ($order->items as $index => $item)
                            <tr>
                                <td class="product-info">
                                    <div class="preview">
                                        <a target="_blank" href="{{ route('products.show', [$item->product_id]) }}">
                                            <img src="{{ $item->product->image_url }}">
                                        </a>
                                    </div>
                                    <div>
                                        <span class="product-title">
                                            <a target="_blank" href="{{ route('products.show', [$item->product_id]) }}">
                                                {{ $item->product->title }}
                                            </a>
                                        </span>
                                        <span class="sku-title">{{ $item->productSku->title }}</span>
                                    </div>
                                </td>
                                <td class="sku-price text-center vertical-middle">${{ $item->price }}</td>
                                <td class="sku-amount text-center vertical-middle">{{ $item->amount }}</td>
                                <td class="item-amount text-right vertical-middle">
                                    ${{ number_format($item->price * $item->amount, 2, '.', '') }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"></td>
                        </tr>
                    </table>
                    <div class="order-bottom">
                        <div class="order-info">
                            <div class="line">
                                <div class="line-label">Address:</div>
                                <div class="line-value">{{ join(' ', $order->address) }}</div>
                            </div>
                            <div class="line">
                                <div class="line-label">Remarks:</div>
                                <div class="line-value">{{ $order->remark ?: '-' }}</div>
                            </div>
                            <div class="line">
                                <div class="line-label">Order No.:</div>
                                <div class="line-value">{{ $order->no }}</div>
                            </div>
                        </div>
                        <div class="order-summary">
                            @if ($order->couponCode)
                            <div class="line">
                                <div class="line-label">
                                    <div class="text-primary"> Discount: </div>
                                </div>
                                <div class="line-value">
                                    <div class="text-primary">{{$order->couponCode->description}}</div>
                                </div>
                            </div>
                            @endif
                            <div class="line">
                                <div class="line-label">
                                    <div class="total-amount"> Total Cost: </div>
                                </div>
                                <div class="line-value">
                                    <div class="total-amount">
                                        <div class="value">${{ $order->total_amount }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line">
                                <div class="line-label">
                                    <div class="text-danger"> Save: </div>
                                </div>
                                <div class="line-value">
                                    <div class="text-danger">
                                        <div class="value">
                                            ${{ is_null($order->couponCode) ? 0 : ($order->couponCode->type === 'fixed' ? $order->couponCode->value :
                                                number_format($order->total_amount /(1 - $order->couponCode->value/100 ) - $order->total_amount, 2, '.', ''))
                                            }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line">
                                <div class="line-label">
                                    <span>Status</span>
                                </div>
                                <div class="line-value">
                                    <div class="value">
                                        @if ($order->paid_at)
                                            @if ($order->refund_status === \App\Models\Order::REFUND_STATUS_PENDING)
                                                Paid
                                            @else
                                                {{ \App\Models\Order::$refundStatusMap[$order->refund_status] }}
                                            @endif
                                        @elseif($order->closed)
                                            Closed
                                        @else
                                            Unpaid
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="line">
                                <div class="line-label">
                                </div>
                                <div class="line-value">
                                    <!-- Payment Button -->
                                    @if (!$order->paid_at && !$order->closed)
                                        <div class="payment-buttons">
                                            <button class="btn btn-primary btn-sm payment-sim" data-id="{{ $order->id }}">
                                                Pay By Simulation
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="note"> Note: This 'Pay By Simulation' button is only for test purpose without integrating any APIs.
                It will not charge you. </span>
        </div>
    </div>
@endsection

@section('scriptsAfterJs')
    <script>
        $(document).ready(function() {
            $('.payment-sim').click(function() {
                var id = $(this).data('id');

                swal({
                    title: "Are you sure to pay now ?",
                    icon: "info",
                    buttons: ['No', 'Yes'],
                }).then((willPay) => {
                    if(!willPay) {
                        return;
                    }
                    axios.put('/payment/' + id, {
                        payment_method:'simulation',
                    }).then(function (response) {
                        //console.log(response.data);
                        location.href = '/payment/' + response.data.id;
                    },function (error) {
                        swal('Internal Error, Please contact admin', '', 'error');
                    })
                })
            })
        })
    </script>
@endsection
