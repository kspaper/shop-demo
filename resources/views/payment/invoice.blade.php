@extends('layouts.app')
@section('title', 'Invoice')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-offset-1">
            <div class="card">
                <div class="card-header">Thanks for shopping with us</div>
                <div class="card-body text-center">
                    <h3 class="invoice-success"> Success !</h3>
                    <h5> Invoice Number: {{ $order->payment_no }} </h5>
                    <a class="btn btn-primary" href="{{ route('root') }}">Back to Home Page</a>
                </div>
            </div>
        </div>
    </div>
@endsection
