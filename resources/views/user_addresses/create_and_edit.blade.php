@extends('layouts.app')
@section('title', ($address->id ? 'Edit' : 'Add') . ' New Address')

@section('content')
    <div class="row">
        <div class="col-md-10 offset-lg-1">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-center">
                        {{ $address->id ? 'Edit' : 'Add' }} New Address
                    </h2>
                </div>
                <div class="card-body">
                    <!-- Output Errors from Server Side -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <h4>Error Occurs: </h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li><i class="glyphicon glyphicon-remove"></i> {{ $error }} </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- End of Output Errors from Server Side -->
                    <!-- Import UserAddressesCreateAndEdit Component -->
                    <user-addresses-create-and-edit inline-template>
                        @if ($address->id)
                            <form class="form-horizontal" role="form"
                                action="{{ route('user_addresses.update', ['user_address' => $address->id]) }}"
                                method="POST">
                                {{ method_field('PUT') }}
                        @else
                            <form class="form-horizontal" role="form"
                                action="{{ route('user_addresses.store') }}" method="post">

                        @endif
                        {{ csrf_field() }}
                        <select-city
                            :init-value="{{ json_encode([
                                old('region', $address->region),
                                old('state', $address->state),
                                old('city', $address->city)]) }}"
                            @change="onCityChanged" inline-template>
                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-md-right">Area</label>
                                <div class="col-sm-3">
                                    <select class="form-control" v-model="regionId">
                                        <option value="">Region</option>
                                        <option v-for="(name, id) in regions" :value="id">@{{ name }} </option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" v-model="stateId">
                                        <option value="">State</option>
                                        <option v-for="(name, id) in states" :value="id">@{{ name }} </option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" v-model="cityId">
                                        <option value="">City</option>
                                        <option v-for="(name, id) in cities" :value="id">@{{ name }} </option>
                                    </select>
                                </div>
                            </div>
                        </select-city>
                        <input type="hidden" name="region" v-model="region">
                        <input type="hidden" name="state" v-model="state">
                        <input type="hidden" name="city" v-model="city">
                        <div class="form-group row">
                            <label class="col-form-label text-md-right col-sm-2">Address</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="address"
                                    value="{{ old('address', $address->address) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-md-right col-sm-2">Postcode</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="zip"
                                    value="{{ old('zip', $address->zip) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-md-right col-sm-2">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="contact_name"
                                    value="{{ old('contact_name', $address->contact_name) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-md-right col-sm-2">Mobile No.</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="contact_phone"
                                    value="{{ old('contact_phone', $address->contact_phone) }}">
                            </div>
                        </div>
                        <div class="form-group row text-center">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </form>
                    </user-addresses-create-and-edit>
                </div>
            </div>
        </div>
    </div>
@endsection
