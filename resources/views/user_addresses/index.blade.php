@extends('layouts.app')
@section('title', 'Address List')

@section('content')
    <div class="row">
        <div class="offset-md-1">
            <div class="card panel-default">
                <div class="card-header">
                    Address List
                    <a href="{{ route('user_addresses.create') }}" class="float-right">Add New Address</a>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Recipients</th>
                                <th>Address</th>
                                <th>Zip Code</th>
                                <th>Mobile No.</th>
                                <th>Operation</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($addresses as $address)
                                <tr>
                                    <td>{{ $address->contact_name }}</td>
                                    <td>{{ $address->full_address }}</td>
                                    <td>{{ $address->zip }}</td>
                                    <td>{{ $address->contact_phone }}</td>
                                    <td>
                                        <a href="{{ route('user_addresses.edit', ['user_address' => $address->id]) }}"
                                            class="btn btn-primary">Change
                                        </a>
                                        <button class="btn btn-danger btn-del-address" type="button" data-id="{{ $address->id }}">
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scriptsAfterJs')
<script>
    $(document).ready(function() {
        $('.btn-del-address').click(function() {
            var id = $(this).data('id');

            swal({
                title: "Are you sure to remove permanently ?",
                icon: "warning",
                buttons: ['No', 'Yes'],
                dangerMode: true,
            })
            .then(function(willDelete) {
                if(!willDelete) {
                    return;
                }
                axios.delete('/user_addresses/' + id)
                .then(function() {
                    location.reload();
                })
            })
        })
    })
</script>
@endsection
