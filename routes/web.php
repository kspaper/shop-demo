<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', 'PagesController@root')->name('root');

// Home page
Route::redirect('/', '/products')->name('root');
Route::get('products', 'ProductsController@index')->name('products.index');
Route::get('products/{product}', 'ProductsController@show')->name('products.show')->where(['product' => '[0-9]+']);

// User Authentication
Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    // Address Routes
    Route::get('user_addresses', 'UserAddressesController@index')->name('user_addresses.index');
    Route::get('user_addresses/create', 'UserAddressesController@create')->name('user_addresses.create');
    Route::post('user_addresses', 'UserAddressesController@store')->name('user_addresses.store');
    Route::get('user_addresses/{user_address}', 'UserAddressesController@edit')->name('user_addresses.edit');
    Route::put('user_addresses/{user_address}', 'UserAddressesController@update')->name('user_addresses.update');
    Route::delete('user_addresses/{user_address}', 'UserAddressesController@destroy')->name('user_addresses.destroy');

    // Favourite Products
    Route::post('products/{product}/favorite', 'ProductsController@favor')->name('products.favor');
    Route::delete('product/{product}/favorite', 'ProductsController@unfavor')->name('products.unfavor');
    Route::get('products/favorites', 'ProductsController@favorites')->name('products.favorites');

    // Shopping Cart
    Route::post('cart','CartController@add')->name('cart.add');
    Route::get('cart', 'CartController@index')->name('cart.index');
    Route::delete('cart/{sku}', 'CartController@remove')->name('cart.remove');

    // Order Routes
    Route::post('orders', 'OrdersController@store')->name('orders.store');
    Route::get('orders', 'OrdersController@index')->name('orders.index');
    Route::get('orders/{order}', 'OrdersController@show')->name('orders.show');
    // Route::get('order/{order}/review', 'OrdersController@review')->name('orders.review.show');
    // Route::post('order/{order}/review', 'OrdersController@sendReview')->name('orders.review.store');

    // Payment Routes
    Route::put('payment/{order}', 'PaymentController@simulatePay')->name('payment.sim');
    Route::get('payment/{order}', 'PaymentController@generateInvoice')->name('payment.invoice');

    Route::get('coupon_codes/{code}', 'CouponCodesController@show')->name('coupon_code.show');

});

